# Irssi Forecast.io Weather Plugin

Announces your weather to the current channel, like this:

    Currently: 54.47F - Mostly Cloudy | Today: 61.23F/50.16F - Light rain until afternoon. | Tomorrow: 66.94F/53.59F - Light rain throughout the day. | Powered by Forecast: http://forecast.io/

Requires the `Forecast::IO` perl module:

`sudo cpan Forecast::IO`

And a (free) [forecast.io API key](https://developer.forecast.io).
