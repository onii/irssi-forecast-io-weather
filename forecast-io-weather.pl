#!/usr/bin/perl
use Forecast::IO;
use strict;
use Irssi qw(command_bind);
use vars qw($VERSION %IRSSI);

$VERSION="20151021";
%IRSSI = (
	authors		=> '',
	contact		=> '',
	name		=> 'forecast_io',
	description	=> 'Prints the current weather.',
	license		=> 'GPL',
);

sub cmd_weather {
    my $lat  = 40.712784;
    my $long = -74.005941;
    my $key = "f93a9fddfd430984857dfc850b423438"; # example Forecast.io API key

    my $forecast = Forecast::IO->new(
        key       => $key,
        longitude => $long,
        latitude  => $lat,
    );

    my $myweather = "Currently: " . $forecast->{currently}->{temperature} . "F - " . $forecast->{currently}->{summary} . " | Today: " . $forecast->{daily}->{data}->[0]->{temperatureMax} . "F/" . $forecast->{daily}->{data}->[0]->{temperatureMin} . "F - " . $forecast->{daily}->{data}->[0]->{summary} . " | Tomorrow: " . $forecast->{daily}->{data}->[1]->{temperatureMax} . "F/" . $forecast->{daily}->{data}->[1]->{temperatureMin} . "F - ". $forecast->{daily}->{data}->[1]->{summary} . " | Powered by Forecast: http://forecast.io/";

    my ($data, $server, $witem) = @_;
    if (!$server || !$server->{connected}) {
      Irssi::print("Not connected to server");
      return;
    }

    if ($data) {
      $server->command("MSG $data $myweather");
    } elsif ($witem && ($witem->{type} eq "CHANNEL" ||
                        $witem->{type} eq "QUERY")) {
      # there's query/channel active in window
      $witem->command("MSG ".$witem->{name}." $myweather");
    } else {
      Irssi::print("Nick not given, and no active channel/query in window");
    }
};

Irssi::command_bind("weather","cmd_weather");
